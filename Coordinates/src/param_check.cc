
/* Copyright 2013 Peter Diener, Nils Dorband, Roland Haas, Ian Hinder,
Christian Ott, Denis Pollney, Thomas Radke, Christian Reisswig, Erik
Schnetter, Barry Wardell and Burkhard Zink

This file is part of Llama.

Llama is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free
Software Foundation, either version 2 of the License, or (at your
option) any later version.

Llama is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License
along with Llama.  If not, see <http://www.gnu.org/licenses/>. */

#include <cctk.h>
#include <cctk_Parameters.h>
#include <cctk_Arguments.h>

namespace Coordinates
{
  extern "C"
  void
  Coordinates_ParamCheck(CCTK_ARGUMENTS)
  {
    DECLARE_CCTK_ARGUMENTS;
    DECLARE_CCTK_PARAMETERS;
    
    if (CCTK_EQUALS (coordinate_system, "Thornburg04") || CCTK_EQUALS (coordinate_system, "Thornburg04nc") || CCTK_EQUALS (coordinate_system, "Thornburg13")) {
      
      if (sphere_outer_radius == 0.)
        CCTK_WARN(0, "Please specify a positive outer boundary radius.");
      
      if (sphere_inner_radius == 0.)
        CCTK_WARN(0, "Please specify a positive sphere inner radius.");
      
      if (sphere_inner_radius > sphere_outer_radius)
        CCTK_WARN
          (0,"sphere_inner_radius should be less than or equal to sphere_outer_radius.");

      if ((radial_stretch) && 
	  ((h_radial_1 < 0) || 
	   (stretch_rmin_1 >= stretch_rmax_1) || 
	   (stretch_rmin_1 < 0) ||
	   (stretch_rmax_1 < 0)))
	    CCTK_WARN(0, "Incorrect specification of stretching region 1.");
	
    }
    if (CCTK_EQUALS (coordinate_system, "Sphere+Column")) {
      if ((radial_stretch) && 
	  ((h_radial_1 < 0) || 
	   (stretch_rmin_1 >= stretch_rmax_1) || 
	   (stretch_rmin_1 < 0) ||
	   (stretch_rmax_1 < 0)))
	    CCTK_WARN(0, "Incorrect specification of stretching region 1.");
	
    }
    if (CCTK_EQUALS (coordinate_system, "Thornburg13")) {
       
       if (sphere_medium_radius > sphere_outer_radius || sphere_medium_radius < sphere_inner_radius)
          CCTK_WARN(0, "sphere medium radius msut be between sphere inner and sphere outer radius!");
       
       if ((radial_stretch) && 
           stretch_rmin_1 < sphere_medium_radius)
           CCTK_WARN(0, "Stretching region must be outside of sphere medium radius.");
    }
  }
}
